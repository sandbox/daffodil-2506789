INTRODUCTION
------------
This module is used for restrict the user from node access.
If they don't have specific role to access the node.
Specific role means, You are making business site and planning to sell 'Roles'

For Eg: If user buy role with "Permium access permission" ,
Only then he can access content otherwise user will send on restriction page.

REQUIREMENTS
------------
This module requires the following modules:
  -Date (https://www.drupal.org/project/date)

Installation
------------
Copy Premium Access module to your sites/all/module directory of Drupal.
Enable on the admin from Modules page.
Configure module setting on /admin/structure/premium_access. 
Set content type and other required options on form.

CONFIGURATION
-------------
There are two option for Premium 'YES' & 'NO'
 -'YES' : Means ,Content is premium and can't be accessible for anonymous user
          Only the user having the specific permission can access contents.
  -'NO'  : It means content will be accessible for all. 

It is also require to set permissions for roles on permission page

MODULE FEATURES:
----------------
  -You can restrict the content from access.
  -You can configure, on which content-type you want to apply restriction.
  -You can set non-premium for particular content during edit
  -You can also update premium access duration unit on edit of content 
  -You can set number of node you want to give free access for anonymous user,
   Currently you can set only one node for free access.

MAINTAINERS
-----------
 * Nitin Goyal  - https://www.drupal.org/u/nitingoyal
 * Sachin Kumar - https://www.drupal.org/u/sachin.k
 * Ritesh Arora - https://www.drupal.org/u/ritesh.daffodil
 * Ankit Aneja  - https://www.drupal.org/u/ankit_daffodilsw
 * Rohit Wadhwa - https://www.drupal.org/u/rohit.wadhwa
 
SPONSORED 
---------
This project has been sponsored by Daffodil:
